## Description
Method 1
getMissingLetters
Takes a String argument and returns a String containing all of the letters from the alphabet
that are not included in the String argument.  


Method 2
animate
Takes an Integer and a String argument.  The Integer is the speed and the String represents the linear
chamber of particles.  The String can only contain the characters ‘R’, ‘L’, or ‘.’.  Particles are marked
by ‘R’ meaning moves right or ‘L’ meaning moves left.  Returns a List of Strings containing each occupied
location at each time step including the last element where the chamber is empty. The length of the
chamber can have a size of 0-50, so I used two Longs to hold the number of the binary string representation of 
the R and L particles.  

Extra Class
RunCode 
RunCode has a main method that calls two methods runMissingLetters() and runAnimate().  Both methods
print out results from calling getMissingLetters() and animate() from the BackEndCodeChallenge class.           


## Installation
Uses gradle to build the java project. To build the project type `./gradlew build`  Navigate to the source folder backendCodeTest and type `./gradlew run`  


## Tests
Type `./gradlew test`