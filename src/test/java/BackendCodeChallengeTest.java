package test.java;

import main.java.BackendCodeChallenge;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Andrew Pleas on 8/27/2016.
 */

public class BackendCodeChallengeTest {

    private BackendCodeChallenge b;


    @Before
    public void initialize() {
        b = new BackendCodeChallenge();
    }

    @Test
    public void getMissingLettersTest() {
        String missing = b.getMissingLetters("A slow yellow fox crawls under the proactive dog");

        assertEquals("bjkmqz", missing);
    }

    @Test
    public void getMissingLettersAllLetters() {
        String missing = b.getMissingLetters("A quick brown fox jumps over the lazy dog");

        assertEquals("", missing);
    }

    @Test
    public void getMissingLettersEmptyStringTest() {
        String missing = b.getMissingLetters("");

        assertEquals("abcdefghijklmnopqrstuvwxyz", missing);
    }

    @Test
    public void getMissingLettersNumberStringTest() {
        String missing = b.getMissingLetters("1234567");

        assertEquals("abcdefghijklmnopqrstuvwxyz", missing);
    }

    @Test
    public void getMissingLettersTest2() {
        String missing = b.getMissingLetters("Lions, and tigers, and bears, oh my!");

        assertEquals("cfjkpquvwxz", missing);
    }
    //getMissingLetters should not accept null strings
    @Test(expected=IllegalArgumentException.class)
    public void getMissingLettersErrorTest () {
        String missing = null;
        b.getMissingLetters(missing);
    }

    @Test
    public void animationTest() {
        List<String> animateList = b.animate(2, "..R....");
        List<String> expectedResult = new ArrayList<String>(){{
            add("..X....");
            add("....X..");
            add("......X");
            add(".......");
        }};


        assertEquals(expectedResult, animateList);
    }

    @Test
    public void animationTest2() {
        List<String> animateList = b.animate(3, "RR..LRL");
        List<String> expectedResult = new ArrayList<String>(){{
            add("XX..XXX");
            add(".X.XX..");
            add("X.....X");
            add(".......");
        }};


        assertEquals(expectedResult, animateList);
    }
    @Test
    public void animationTest3() {
        List<String> animateList = b.animate(2, "LRLR.LRLR");
        List<String> expectedResult = new ArrayList<String>(){{
            add("XXXX.XXXX");
            add("X..X.X..X");
            add(".X.X.X.X.");
            add(".X.....X.");
            add(".........");
        }};

        assertEquals(expectedResult, animateList);
    }

    @Test
    public void animationTest4() {
        List<String> animateList = b.animate(10, "RLRLRLRLRL");
        List<String> expectedResult = new ArrayList<String>(){{
            add("XXXXXXXXXX");
            add("..........");
        }};

        assertEquals(expectedResult, animateList);
    }

    @Test
    public void animationTest5() {
        List<String> animateList = b.animate(1, "...");
        List<String> expectedResult = new ArrayList<String>(){{
            add("...");

        }};

        assertEquals(expectedResult, animateList);
    }

    @Test
    public void animationTest6() {
        List<String> animateList = b.animate(1, "LRRL.LR.LRR.R.LRRL.");
        List<String> expectedResult = new ArrayList<String>(){{
            add("XXXX.XX.XXX.X.XXXX.");
            add("..XXX..X..XX.X..XX.");
            add(".X.XX.X.X..XX.XX.XX");
            add("X.X.XX...X.XXXXX..X");
            add(".X..XXX...X..XX.X..");
            add("X..X..XX.X.XX.XX.X.");
            add("..X....XX..XX..XX.X");
            add(".X.....XXXX..X..XX.");
            add("X.....X..XX...X..XX");
            add(".....X..X.XX...X..X");
            add("....X..X...XX...X..");
            add("...X..X.....XX...X.");
            add("..X..X.......XX...X");
            add(".X..X.........XX...");
            add("X..X...........XX..");
            add("..X.............XX.");
            add(".X...............XX");
            add("X.................X");
            add("...................");
        }};

        assertEquals(expectedResult, animateList);
    }
    //animate should not accept null strings
    @Test(expected=IllegalArgumentException.class)
    public void animationErrorTest() {
        String badString = null;
      b.animate(1, badString);

    }
    //animate should not accept empty strings
    @Test(expected=IllegalArgumentException.class)
    public void animationErrorTest2() {
        String badString = "";
        b.animate(1, badString);

    }
    //animate only accepts strings that contain 'R', 'L' or '.'
    @Test(expected=IllegalArgumentException.class)
    public void animationErrorTest3() {
        String badString = "123134124";
        b.animate(2, badString);
    }

    //animate throws an error if the String length is greater or equal to 64
    @Test(expected=IllegalArgumentException.class)
    public void animationErrorTest5() {
        String badString = "RLRLRL...RRLRLRL...RRLRLRL...RRLRLRL...RRLRLRL...RLLLLRLRLRL...L";
        b.animate(2, badString);
    }



}

