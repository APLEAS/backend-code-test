package main.java;

import java.util.*;

/**
 * Created by Andrew Pleas on 8/28/2016.
 */


public class RunCode {

    public static void main(String[] args){
       runMissingLetters();
       runAnimate();
    }

    private static void runAnimate() {
        BackendCodeChallenge b = new BackendCodeChallenge();
        List<String> one = new ArrayList<String>(){{
            add("...");
            add("LRRL.LR.LRR.R.LRRL.");
        }};
        List<String> two = new ArrayList<String>(){{
            add("..R....");
            add("LRLR.LRLR");
        }};
        List<String> three = new ArrayList<String>(){{
            add("RR..LRL");
        }};

        List<String> ten = new ArrayList<String>(){{
            add("RLRLRLRLRL");
        }};

        Map<Integer, List<String>> particleMap = new HashMap<>();
        particleMap.put(1, one);
        particleMap.put(2, two);
        particleMap.put(3, three);
        particleMap.put(10, ten);

        for(Map.Entry<Integer, List<String>> set : particleMap.entrySet()){
            int speed = set.getKey();
            List<String> particleList = set.getValue();
            for(String particle: particleList) {
                List<String> resultList = b.animate(speed, particle);
                System.out.println("Results for speed = " + speed + ": particle = " + particle);
                System.out.println("==================================================");
                for (String result : resultList) {
                    System.out.println(result);
                }
                System.out.println();
            }
        }

    }

    private static void runMissingLetters() {
        BackendCodeChallenge b = new BackendCodeChallenge();
        List<String> sentenceList = new ArrayList<String>(){{
            add("A quick brown fox jumps over the lazy dog");
            add("A slow yellow fox crawls under the proactive dog");
            add("Lions, and tigers, and bears, oh my!");
            add("");
        }};

        for(String s: sentenceList){
            System.out.println("\""+s+"\""  + ": missing letters = [" + b.getMissingLetters(s)+"]");
        }
        System.out.println();
    }
}
