package main.java;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew Pleas on 8/27/2016.
 */


public class BackendCodeChallenge {

    /**
     * getMissingLetters returns a String containing all of letters that are not
     * in the String argument.  Casing is ignored, all letters are converted to lowercase.
     * Only shows letters that are US-ASCII characters.
     */


    /**
     * @param init a String containing a sentence
     * @return a String containing all of the letters not contained in init
     */
    public String getMissingLetters(String init) {
        if (init == null) {
            throw new IllegalArgumentException("init cannot be null.");
        }
        String lowerInit = init.toLowerCase();
        String result = "";
        boolean[] alphaList = new boolean[26];
        for (int i = 0; i < lowerInit.length(); i++) {
            if ((int) lowerInit.charAt(i) >= 97 && lowerInit.charAt(i) <= 122) {
                alphaList[((int) lowerInit.charAt(i)) - 97] = true;
            }
        }
        for (int j = 0; j < alphaList.length; j++) {
            if (!alphaList[j]) {
                result += (char) (j + 97);
            }
        }
        return result;
    }

    /**
     * animate returns a String List containing the states
     * of the particles as they move through the linear chamber.
     * The String argument must contain 'R', 'L' or '.' characters
     * (other characters will throw an IllegalArgumentException).
     */

    /**
     * @param init a String containing a sentence
     * @return a String containing all of the letters not contained in init
     */
    public List<String> animate(int speed, String init) throws IllegalArgumentException {
        if (init == null || init.isEmpty()) {
            throw new IllegalArgumentException("init cannot be null or empty");
        }
        if (!init.matches("^[RL.]+$")) {
            throw new IllegalArgumentException("Init can only contain the characters R, L or .");
        }
        if (init.length() >= 64) {
            throw new IllegalArgumentException("init must have less than 64 characters  ");
        }
        List<String> resultList = new ArrayList<>();
        boolean clear = false;
        StringBuilder rightBinary = new StringBuilder();
        StringBuilder leftBinary = new StringBuilder();
        if (!(init.contains("R") || init.contains("L"))) {
            resultList.add(init);
            return resultList;
        }
        for (int k = 0; k < init.length(); k++) {
            if (init.charAt(k) == 'R') {
                rightBinary.append('1');
            } else {
                rightBinary.append('0');
            }
            if (init.charAt(k) == 'L') {
                leftBinary.append('1');
            } else {
                leftBinary.append('0');
            }
        }
        Long rightLong = Long.parseLong(rightBinary.toString(), 2);
        Long leftLong = Long.parseLong(leftBinary.toString(), 2);
        String result = Long.toBinaryString((leftLong | rightLong));
        result = init.replaceAll("L", "X");
        result = result.replaceAll("R", "X");
        resultList.add(result);
        while (!clear) {
            rightLong = rightLong >> speed;
            leftLong = leftLong << speed;
            Long combined = (leftLong | rightLong);
            result = Long.toBinaryString(combined);
            if (result.length() > init.length()) {
                String left = Long.toBinaryString(leftLong);
                left = left.substring(left.length() - init.length());
                leftLong = Long.parseLong(left, 2);
                result = result.substring(result.length() - init.length());
            }
            combined = Long.parseLong(result, 2);
            if (combined <= 0) {
                clear = true;
            }
            result = result.replaceAll("0", ".");
            result = result.replaceAll("1", "X");
            int diff = init.length() - result.length();
            if (diff > 0) {
                StringBuilder buildResult = new StringBuilder(result);
                for (int k = 0; k < diff; k++) {
                    buildResult.insert(0, '.');
                    result = buildResult.toString();
                }
            }
            resultList.add(result);
        }
        return resultList;
    }
}